package org.fmavlyutov.controller;

import org.fmavlyutov.api.controller.IProjectController;
import org.fmavlyutov.api.service.IProjectService;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.println("Enter name:");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        String description = TerminalUtil.nextLine();
        Project project = projectService.create(name, description);
        if (project == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showProject(project);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void clearProjects() {
        System.out.println("[CLEAR PROJECTS]");
        projectService.clear();
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void showProjects() {
        System.out.println("[PROJECTS LIST]");
        List<Project> projects = projectService.findAll();
        int index = 1;
        for (Project project : projects) {
            if (project == null) {
                continue;
            }
            System.out.println(index++ + ". " + project.getName());
        }
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void showProjectById() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("Enter id:");
        String id = TerminalUtil.nextLine();
        Project project = projectService.findOneById(id);
        if (project == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showProject(project);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("[SHOW PROJECT]");
        System.out.println("Enter index:");
        Integer index = TerminalUtil.nextInt() - 1;
        Project project = projectService.findOneByIndex(index);
        if (project == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showProject(project);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void updateProjectById() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("Enter id:");
        String id = TerminalUtil.nextLine();
        System.out.println("Enter name:");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        String description = TerminalUtil.nextLine();
        Project project = projectService.updateById(id, name, description);
        if (project == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showProject(project);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void updateProjectByIndex() {
        System.out.println("[UPDATE PROJECT]");
        System.out.println("Enter index:");
        Integer index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter name:");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        String description = TerminalUtil.nextLine();
        Project project = projectService.updateByIndex(index, name, description);
        if (project == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showProject(project);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void removeProjectById() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("Enter id:");
        String id = TerminalUtil.nextLine();
        Project project = projectService.removeById(id);
        if (project == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void removeProjectByIndex() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("Enter index:");
        Integer index = TerminalUtil.nextInt() - 1;
        Project project = projectService.removeByIndex(index);
        if (project == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        System.out.println("[SUCCESSFUL]\n");
    }

    private void showProject(Project project) {
        if (project == null) {
            return;
        }
        System.out.println(project);
    }

    @Override
    public void startProjectById() {
        System.out.println("[START PROJECT]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeStatusById(id, Status.IN_PROGRESS);
        if (project == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showProject(project);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void startProjectByIndex() {
        System.out.println("[START PROJECT]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Project project = projectService.changeStatusByIndex(index, Status.IN_PROGRESS);
        if (project == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showProject(project);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void completeProjectById() {
        System.out.println("[COMPLETE PROJECT]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Project project = projectService.changeStatusById(id, Status.COMPLETED);
        if (project == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showProject(project);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void completeProjectByIndex() {
        System.out.println("[COMPLETE PROJECT]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Project project = projectService.changeStatusByIndex(index, Status.COMPLETED);
        if (project == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showProject(project);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void changeProjectStatusById() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("Enter index:");
        Integer index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String status = TerminalUtil.nextLine();
        final Project project = projectService.changeStatusByIndex(index, Status.toStatus(status));
        if (project == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showProject(project);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void changeProjectStatusByIndex() {
        System.out.println("[CHANGE PROJECT STATUS]");
        System.out.println("Enter index:");
        String id = TerminalUtil.nextLine();
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String status = TerminalUtil.nextLine();
        final Project project = projectService.changeStatusById(id, Status.toStatus(status));
        if (project == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showProject(project);
        System.out.println("[SUCCESSFUL]\n");
    }

}
