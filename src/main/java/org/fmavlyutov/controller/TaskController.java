package org.fmavlyutov.controller;

import org.fmavlyutov.api.controller.ITaskController;
import org.fmavlyutov.api.service.ITaskService;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("Enter name:");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        String description = TerminalUtil.nextLine();
        Task task = taskService.create(name, description);
        if (task == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showTask(task);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void showTasks() {
        System.out.println("[TASKS LIST]");
        List<Task> tasks = taskService.findAll();
        int index = 1;
        for (Task task : tasks) {
            if (task == null) {
                continue;
            }
            System.out.println(index++ + ". " + task.getName());
        }
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("Enter id:");
        String id = TerminalUtil.nextLine();
        Task task = taskService.findOneById(id);
        if (task == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showTask(task);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("Enter index:");
        Integer index = TerminalUtil.nextInt() - 1;
        Task task = taskService.findOneByIndex(index);
        if (task == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showTask(task);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("Enter id:");
        String id = TerminalUtil.nextLine();
        System.out.println("Enter name:");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        String description = TerminalUtil.nextLine();
        Task task = taskService.updateById(id, name, description);
        if (task == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showTask(task);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("Enter index:");
        Integer index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter name:");
        String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        String description = TerminalUtil.nextLine();
        Task task = taskService.updateByIndex(index, name, description);
        if (task == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showTask(task);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK]");
        System.out.println("Enter id:");
        String id = TerminalUtil.nextLine();
        Task task = taskService.removeById(id);
        if (task == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK]");
        System.out.println("Enter index:");
        Integer index = TerminalUtil.nextInt() - 1;
        Task task = taskService.removeByIndex(index);
        if (task == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        System.out.println("[SUCCESSFUL]\n");
    }

    private void showTask(Task task) {
        if (task == null) {
            return;
        }
        System.out.println(task);
    }

    @Override
    public void startTaskById() {
        System.out.println("[COMPLETE TASK]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusById(id, Status.COMPLETED);
        if (task == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showTask(task);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Task task = taskService.changeStatusByIndex(index, Status.IN_PROGRESS);
        if (task == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showTask(task);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void completeTaskById() {
        System.out.println("[COMPLETE TASK]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusById(id, Status.COMPLETED);
        if (task == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showTask(task);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[COMPLETE TASK]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Task task = taskService.changeStatusByIndex(index, Status.COMPLETED);
        if (task == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showTask(task);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("Enter index:");
        Integer index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String status = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusByIndex(index, Status.toStatus(status));
        if (task == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showTask(task);
        System.out.println("[SUCCESSFUL]\n");
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("Enter index:");
        String id = TerminalUtil.nextLine();
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String status = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusById(id, Status.toStatus(status));
        if (task == null) {
            System.out.println("[UNSUCCESSFUL]\n");
            return;
        }
        showTask(task);
        System.out.println("[SUCCESSFUL]\n");
    }

}
