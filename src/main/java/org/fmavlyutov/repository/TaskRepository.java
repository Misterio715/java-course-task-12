package org.fmavlyutov.repository;

import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(Task task) {
        tasks.add(task);
        return task;
    }

    @Override
    public void clear() {
        tasks.clear();
    }

    @Override
    public List<Task> findAll() {
        return tasks;
    }

    @Override
    public Task findOneById(String id) {
        for (Task task : tasks) {
            if (id.equals(task.getId())) {
                return task;
            }
        }
        return null;
    }

    @Override
    public Task findOneByIndex(Integer index) {
        return tasks.get(index);
    }

    @Override
    public void remove(Task task) {
        tasks.remove(task);
    }

    @Override
    public Task removeById(String id) {
        Task task = findOneById(id);
        if (task == null) {
            return null;
        }
        remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(Integer index) {
        Task task = findOneByIndex(index);
        if (task == null) {
            return null;
        }
        remove(task);
        return task;
    }

    @Override
    public Integer getSize() {
        return tasks.size();
    }

}
