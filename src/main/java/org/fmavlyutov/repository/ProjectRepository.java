package org.fmavlyutov.repository;

import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public Project add(Project project) {
        projects.add(project);
        return project;
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public List<Project> findAll() {
        return projects;
    }

    @Override
    public Project findOneById(String id) {
        for (Project project : projects) {
            if (id.equals(project.getId())) {
                return project;
            }
        }
        return null;
    }

    @Override
    public Project findOneByIndex(Integer index) {
        return projects.get(index);
    }

    @Override
    public void remove(Project project) {
        projects.remove(project);
    }

    @Override
    public Project removeById(String id) {
        Project project = findOneById(id);
        if (project == null) {
            return null;
        }
        remove(project);
        return project;
    }

    @Override
    public Project removeByIndex(Integer index) {
        Project project = findOneByIndex(index);
        if (project == null) {
            return null;
        }
        remove(project);
        return project;
    }

    @Override
    public Integer getSize() {
        return projects.size();
    }

}
