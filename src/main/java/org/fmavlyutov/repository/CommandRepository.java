package org.fmavlyutov.repository;

import org.fmavlyutov.api.repository.ICommandRepository;
import org.fmavlyutov.constant.CommandLineArgument;
import org.fmavlyutov.constant.CommandLineConstant;
import org.fmavlyutov.model.Command;

public final class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(CommandLineConstant.HELP, CommandLineArgument.HELP, "display info about commands and arguments");

    public static final Command VERSION = new Command(CommandLineConstant.VERSION, CommandLineArgument.VERSION, "display program version");

    public static final Command ABOUT = new Command(CommandLineConstant.ABOUT, CommandLineArgument.ABOUT, "display info about author");

    public static final Command INFO = new Command(CommandLineConstant.INFO, CommandLineArgument.INFO, "display system info");

    public static final Command COMMANDS = new Command(CommandLineConstant.COMMANDS, CommandLineArgument.COMMANDS, "display all commands");

    public static final Command ARGUMENTS = new Command(CommandLineConstant.ARGUMENTS, CommandLineArgument.ARGUMENTS, "display all arguments");

    public static final Command EXIT = new Command(CommandLineConstant.EXIT, null, "finish program");

    public static final Command PROJECT_CREATE = new Command(CommandLineConstant.PROJECT_CREATE, null,"create new project");

    public static final Command PROJECT_LIST = new Command(CommandLineConstant.PROJECT_LIST, null, "show list of projects");

    public static final Command PROJECT_CLEAR = new Command(CommandLineConstant.PROJECT_CLEAR, null, "delete all projects");

    public static final Command PROJECT_SHOW_BY_ID = new Command(CommandLineConstant.PROJECT_SHOW_BY_ID, null, "show project by id");

    public static final Command PROJECT_SHOW_BY_INDEX = new Command(CommandLineConstant.PROJECT_SHOW_BY_INDEX, null, "show project by index");

    public static final Command PROJECT_UPDATE_BY_ID = new Command(CommandLineConstant.PROJECT_UPDATE_BY_ID, null, "update project by id");

    public static final Command PROJECT_UPDATE_BY_INDEX = new Command(CommandLineConstant.PROJECT_UPDATE_BY_INDEX, null, "update project by index");

    public static final Command PROJECT_REMOVE_BY_ID = new Command(CommandLineConstant.PROJECT_REMOVE_BY_ID, null, "delete project by id");

    public static final Command PROJECT_REMOVE_BY_INDEX = new Command(CommandLineConstant.PROJECT_REMOVE_BY_INDEX, null, "delete project by index");

    public static final Command PROJECT_START_BY_ID = new Command(CommandLineConstant.PROJECT_START_BY_ID, null, "start project by id");

    public static final Command PROJECT_START_BY_INDEX = new Command(CommandLineConstant.PROJECT_START_BY_INDEX, null, "start project by index");

    public static final Command PROJECT_COMPLETE_BY_ID = new Command(CommandLineConstant.PROJECT_COMPLETE_BY_ID, null, "complete project by id");

    public static final Command PROJECT_COMPLETE_BY_INDEX = new Command(CommandLineConstant.PROJECT_COMPLETE_BY_INDEX, null, "complete project by index");

    public static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(CommandLineConstant.PROJECT_CHANGE_STATUS_BY_ID, null, "change project status by id");

    public static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(CommandLineConstant.PROJECT_CHANGE_STATUS_BY_INDEX, null, "change project status by index");

    public static final Command TASK_CREATE = new Command(CommandLineConstant.TASK_CREATE, null,"create new task");

    public static final Command TASK_LIST = new Command(CommandLineConstant.TASK_LIST, null, "show list of tasks");

    public static final Command TASK_CLEAR = new Command(CommandLineConstant.TASK_CLEAR, null, "delete all tasks");

    public static final Command TASK_SHOW_BY_ID = new Command(CommandLineConstant.TASK_SHOW_BY_ID, null, "show task by id");

    public static final Command TASK_SHOW_BY_INDEX = new Command(CommandLineConstant.TASK_SHOW_BY_INDEX, null, "show task by index");

    public static final Command TASK_UPDATE_BY_ID = new Command(CommandLineConstant.TASK_UPDATE_BY_ID, null, "update task by id");

    public static final Command TASK_UPDATE_BY_INDEX = new Command(CommandLineConstant.TASK_UPDATE_BY_INDEX, null, "update task by index");

    public static final Command TASK_REMOVE_BY_ID = new Command(CommandLineConstant.TASK_REMOVE_BY_ID, null, "delete task by id");

    public static final Command TASK_REMOVE_BY_INDEX = new Command(CommandLineConstant.TASK_REMOVE_BY_INDEX, null, "delete task by index");

    public static final Command TASK_START_BY_ID = new Command(CommandLineConstant.TASK_START_BY_ID, null, "start task by id");

    public static final Command TASK_START_BY_INDEX = new Command(CommandLineConstant.TASK_START_BY_INDEX, null, "start task by index");

    public static final Command TASK_COMPLETE_BY_ID = new Command(CommandLineConstant.TASK_COMPLETE_BY_ID, null, "complete task by id");

    public static final Command TASK_COMPLETE_BY_INDEX = new Command(CommandLineConstant.TASK_COMPLETE_BY_INDEX, null, "complete task by index");

    public static final Command TASK_CHANGE_STATUS_BY_ID = new Command(CommandLineConstant.TASK_CHANGE_STATUS_BY_ID, null, "change task status by id");

    public static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(CommandLineConstant.TASK_CHANGE_STATUS_BY_INDEX, null, "change task status by index");

    public static final Command[] TERMINAL_COMMANDS = new Command[] {
            HELP, VERSION, ABOUT, INFO, EXIT, COMMANDS, ARGUMENTS,

            PROJECT_CREATE, PROJECT_LIST, PROJECT_CLEAR, PROJECT_SHOW_BY_ID, PROJECT_SHOW_BY_INDEX, PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX, PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX, PROJECT_COMPLETE_BY_ID, PROJECT_COMPLETE_BY_INDEX, PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX,

            TASK_CREATE, TASK_LIST, TASK_CLEAR, TASK_SHOW_BY_ID, TASK_SHOW_BY_INDEX, TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX, TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_START_BY_ID, TASK_START_BY_INDEX, TASK_COMPLETE_BY_ID, TASK_COMPLETE_BY_INDEX, TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX,
    };

    public Command[] getCommands() {
        return TERMINAL_COMMANDS;
    }

}
