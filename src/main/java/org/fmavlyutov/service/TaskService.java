package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.api.service.ITaskService;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(Task task) {
        if (task == null) {
            return null;
        }
        return taskRepository.add(task);
    }

    @Override
    public void clear() {
        taskRepository.clear();
    }

    @Override
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    @Override
    public Task create(String name, String description) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        Task task = new Task();
        task.setName(name);
        if (description != null) {
            task.setDescription(description);
        }
        return add(task);
    }

    @Override
    public Task findOneById(String id) {
        if (id == null || id.isEmpty()) {
            return null;
        }
        return taskRepository.findOneById(id);
    }

    @Override
    public Task findOneByIndex(Integer index) {
        if (index == null || index < 0 || index >= taskRepository.getSize()) {
            return null;
        }
        return taskRepository.findOneByIndex(index);
    }

    @Override
    public Task updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) {
            return null;
        }
        if (name == null | name.isEmpty()) {
            return null;
        }
        Task task = findOneById(id);
        if (task == null) {
            return null;
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public Task updateByIndex(Integer index, String name, String description) {
        if (index == null || index < 0 || index >= taskRepository.getSize()) {
            return null;
        }
        if (name == null | name.isEmpty()) {
            return null;
        }
        Task task = findOneByIndex(index);
        if (task == null) {
            return null;
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    public void remove(Task task) {
        if (task == null) {
            return;
        }
        taskRepository.remove(task);
    }

    @Override
    public Task removeById(String id) {
        if (id == null || id.isEmpty()) {
            return null;
        }
        return taskRepository.removeById(id);
    }

    @Override
    public Task removeByIndex(Integer index) {
        if (index == null || index < 0 || index >= taskRepository.getSize()) {
            return null;
        }
        return taskRepository.removeByIndex(index);
    }

    @Override
    public Task changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) {
            return null;
        }
        final Task task = taskRepository.findOneById(id);
        if (task == null) {
            return null;
        }
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= taskRepository.getSize()) {
            return null;
        }
        final Task task = taskRepository.findOneByIndex(index);
        if (task == null) {
            return null;
        }
        task.setStatus(status);
        return task;
    }

}
