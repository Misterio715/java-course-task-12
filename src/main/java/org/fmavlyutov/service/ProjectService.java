package org.fmavlyutov.service;

import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.api.service.IProjectService;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Project;

import java.util.List;

public final class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(Project project) {
        if (project == null) {
            return null;
        }
        return projectRepository.add(project);
    }

    @Override
    public void clear() {
        projectRepository.clear();
    }

    @Override
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    @Override
    public Project create(String name, String description) {
        if (name == null || name.isEmpty()) {
            return null;
        }
        Project project = new Project();
        project.setName(name);
        if (description != null) {
            project.setDescription(description);
        }
        return add(project);
    }

    @Override
    public Project findOneById(String id) {
        if (id == null || id.isEmpty()) {
            return null;
        }
        return projectRepository.findOneById(id);
    }

    @Override
    public Project findOneByIndex(Integer index) {
        if (index == null || index < 0 || index >= projectRepository.getSize()) {
            return null;
        }
        return projectRepository.findOneByIndex(index);
    }

    @Override
    public Project updateById(String id, String name, String description) {
        if (id == null || id.isEmpty()) {
            return null;
        }
        if (name == null | name.isEmpty()) {
            return null;
        }
        Project project = findOneById(id);
        if (project == null) {
            return null;
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(Integer index, String name, String description) {
        if (index == null || index < 0 || index >= projectRepository.getSize()) {
            return null;
        }
        if (name == null | name.isEmpty()) {
            return null;
        }
        Project project = findOneByIndex(index);
        if (project == null) {
            return null;
        }
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public void remove(Project project) {
        if (project == null) {
            return;
        }
        projectRepository.remove(project);
    }

    @Override
    public Project removeById(String id) {
        if (id == null || id.isEmpty()) {
            return null;
        }
        return projectRepository.removeById(id);
    }

    @Override
    public Project removeByIndex(Integer index) {
        if (index == null || index < 0 || index >= projectRepository.getSize()) {
            return null;
        }
        return projectRepository.removeByIndex(index);
    }

    @Override
    public Project changeStatusById(final String id, final Status status) {
        if (id == null || id.isEmpty()) {
            return null;
        }
        final Project project = projectRepository.findOneById(id);
        if (project == null) {
            return null;
        }
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeStatusByIndex(final Integer index, final Status status) {
        if (index == null || index < 0 || index >= projectRepository.getSize()) {
            return null;
        }
        final Project project = projectRepository.findOneByIndex(index);
        if (project == null) {
            return null;
        }
        project.setStatus(status);
        return project;
    }

}
