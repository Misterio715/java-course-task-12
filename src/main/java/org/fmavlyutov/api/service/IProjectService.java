package org.fmavlyutov.api.service;

import org.fmavlyutov.api.repository.IProjectRepository;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Project;

import java.util.List;

public interface IProjectService {

    Project add(Project project);

    void clear();

    List<Project> findAll();

    Project create(String name, String description);

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    Project updateById(String id, String name, String description);

    Project updateByIndex(Integer index, String name, String description);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

    Project changeStatusById(String id, Status status);

    Project changeStatusByIndex(Integer index, Status status);

}
