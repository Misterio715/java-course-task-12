package org.fmavlyutov.api.service;

import org.fmavlyutov.api.repository.ITaskRepository;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Project;
import org.fmavlyutov.model.Task;

import java.util.List;

public interface ITaskService {

    Task add(Task task);

    void clear();

    List<Task> findAll();

    Task create(String name, String description);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task changeStatusById(String id, Status status);

    Task changeStatusByIndex(Integer index, Status status);

}
